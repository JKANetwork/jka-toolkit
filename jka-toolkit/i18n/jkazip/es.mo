��          �      |      �      �          (     E  	   a  N   k  T   �  %        5  /   H     x     �     �  #   �  %   �  (        C  "   b     �  !   �  !   �  p  �     I     i      �  #   �  	   �  U   �  X   )  #   �     �  3   �  '   �  #     !   <  .   ^  1   �  /   �  )   �  +   	     E	  #   Y	  5   }	                                            	                          
                           	-c <output file>,	compress file 	-d,		decompress file 	-h,		show help box and exit 	-v,		show version and exit 	Examples 	Supported formats: tar, gzip, bzip2, xzip, lzip, 7z, rar (only unzip) and zip 	The order must be "jkazip -d <file>" or "jkazip -c <output> <files or directories>" 	jkazip -c ouputfile.tar.gz directory 	jkazip -d file.7z 	synopsis:	jkazip <options> <file or directory> %s no such file or directory File %s has been decompressed File %s will be decompressed File extension %s are not supported File extension haven't been specified File/s %s has/have been compressed in %s No file or directory specified No option specified, use jkazip -h Unknown option jkazip by JKA Network; version %s rar compression are not supported Project-Id-Version: 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-06-17 10:55+0200
PO-Revision-Date: 2016-06-17 11:02+0200
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.7.1
Last-Translator: JoseluCross <jlgarrido97@gmail.com>
Plural-Forms: nplurals=2; plural=(n != 1);
Language: es
 	-c <salida>,	comprimir archivo 	-d,		descomprimir archivo 	-h,		muestra la ayuda y termina 	-v,		muestra la versión y termina 	Ejemplos 	Formatos soportados: tar, gzip, bzip2, xzip, lzip, 7z, rar (solo descomprimir) y zip 	El orden debe ser "jkazip -d <archivo>" o "jkazip -c <salida> <archivos o directorios>" 	jkazip -c salida.tar.gz directorio 	jkazip -d archivo.7z 	sinopsis:	jkazip <opciones> <archivo o directorio> %s no existe el archivo o el directorio El archivo %s ha sido descomprimido El archivo %s será descomprimido La extensión de archivo %s no está soportada La extensión del archivo no ha sido especificada El/Los archivo/s %s ha/n sido comprimidos en %s Ningún archivo o directorio especificado Ninguna opción especificada, use jkazip -h Opción desconocida jkazip por JKA Network; versión %s El formato rar no está soportado para la compresión 