### JKA Toolkit

GENERAL USAGE NOTES
--------------------
--------------------
* JKA Toolkit is a set of script to make easiest of Linux terminal
* Now this kit include:
	* apw - a package wrapper - Version 1.9.1
		* A wrapper to manage package managers of the differents Linux distributions, compatible with the families of *Debian, Archlinux, RedHat, CentOS, OpenSuse* and *Void Linux*
	* jkazip - Version 2.0.3
		* A script which use the differents zip and unzip programs, support *tar, lzip, lzma (xz), zip, 7zip, gzip, bzip2* and *rar* (only for unzip)
	* gitdit - GIT DIalog inTerface Version 1.0.2
		* A dialog interface to manage git repositories with *pull,  add-commit-push, clone, checkout,* and *merge*

* As dependences, it needs "gettext" for make translations work

Contact
-------
* Web Site: proyecto.jkanetwork.com
* E-mail:	  contacto@jkanetwork.com

License
-------
* GPLv3
* READ LICENSE
