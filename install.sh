#!/bin/bash

echo "Este instalador copiará los scripts a /usr/bin/ y les dejara permisos de ejecución"
echo "This installer will copy the scripts to /usr/bin and make executable"
#Force sudo
uid=$(/usr/bin/id -u)
if [[ $uid != "0" ]];then
echo "installer has to run as root / with sudo"
exit
fi

cp jka-toolkit/{apw,jkazip,gitdit} /usr/bin/
chmod +x /usr/bin/apw
chmod +x /usr/bin/jkazip
chmod +x /usr/bin/gitdit
#Languages
cp jka-toolkit/i18n/apw/es.mo /usr/share/locale/es/LC_MESSAGES/apw.mo
cp jka-toolkit/i18n/jkazip/es.mo /usr/share/locale/es/LC_MESSAGES/jkazip.mo
#manpages
cp manpages/apw.1.gz /usr/share/man/man1/
sudo mandb -q
echo "Finalizado"
echo "Finished"
